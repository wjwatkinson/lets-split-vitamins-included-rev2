# Building a Let's Split Vitamins Included Rev.2

This repository contains instructions for [building](/instructions/building.md), [creating custom firmware](/instructions/creating-custom-firmware.md) and [flashing](/instructions/flashing.md) a Let's Split Vitamins Included Rev.2.

It also contains a copy of the custom keymap I created for my keyboard in the keymaps folder and the .hex file for it in the keymaps/build folder.