# Creating Custom Firmware

## Requirements
- Unix terminal
- [QMK](https://docs.qmk.fm/#/newbs_getting_started?id=set-up-your-environment)
- Download the [qmk_firmware](https://github.com/qmk/qmk_firmware) repository

## Process
1. In your terminal navigate into the qmk_firmware repository you downloaded above.
2. Once there navigate to keyboards/vitamins_included/keymaps.
3. In the keymaps folder there will be a folder called default, which contains the default layout. The easiest way to make a custom layout is by copying the default and modifying it.
4. Copy the default folder into one with whatever name you want, `cp -r default your-folder-name`.
5. Customize the keymap.c file to the layout you want.
6. From your newly created folder containing your modified keymap.c and config.h run `qmk compile`. This command creates firmware files in a .build folder in the qmk_firmware root directory.
7. Navigate back to the qmk_firmware repository and go into the .build folder. This folder may not be visibile since it begins with a ".", but you can navigate into it using `cd .build`
8. The .hex file in here is what you will need to flash your keyboard with your custom layout.

## Notes

I got tripped up creating my firmware becuase I was using the incorrect keyboard. Within qmk_firmware/keyboards there are lets_split and lets_split_eh folders. The correct keyboard is in the vitamins_included folder.