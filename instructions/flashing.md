# Flashing

## Requirements
- macOS or Windows
- [QMK Toolbox](https://github.com/qmk/qmk_toolbox/releases/tag/0.0.17)
- Vitamins Included Rev.2 .hex file

## Process

Only the half of the keyboard plugged in via USB-C is flashed, so steps 3-5

1. Open the QMK Toolbox.
2. Select your .hex file in the Local file field.
(The steps below need to be repeated for both halves of the keybaord)
3. Connect half of the keyboard to the computer via USB-C.
4. Press the small white button protruding from the bottom of the PCB that says reset near it. #11 in the diagram below.
![alt text](/img/pcb-bottom.PNG "PCB Bottom")
5. Click the Flash button when a yellow message starting with `*** DFU device connected: ...` will appears.
6. Plug in the other half of your keyboard directly into your computer and repeat steps 3-5.

## Notes

### Linux
QMK provides a way to [flash keyboards using the command line](https://docs.qmk.fm/#/newbs_flashing?id=flash-your-keyboard-from-the-command-line), which should work on linux, but I have not been able to get it to recognize my keyboard is plugged in. I am going to continue experimenting with this.

### Experience
I have only done this on Windows, but the QMK Toolbox is also available for macOS, so it should be similar. If you discover it is different please let me know and I will make appropriate updates.