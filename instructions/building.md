# Building a Vitamins Included Rev.2

## Components
- [Printed Circuit Board (PCB)](https://novelkeys.xyz/products/vitamins-included-rev-2)
- 48 mechanical switches
- 48 keycaps
- 8 m.3 screws and bolts
- USB-C to USB-A cable
- TRS or TRRS cable (AUX cable)
- Access to a 3D printer

## Printing the Case

1. Download the left and right case files from [qmk.fm](https://qmk.fm/keyboards/vitamins_included#3d-printable-files)
2. Print the case. This will vary depending on the printer you are using.

### Form 2 Notes
Orientation - I had PreForm calculate the orientation and did not have any issues, but I am not sure it is better than just printing flat and adding supports. It also takes longer and requires more resin.

Supports - I did all of my prints using supports as recommended.

Resin - I printed in black, white and tough 2000, all of which worked, so I do not have a specific recommendation.

Finishing - I ran into issues with the bolt holes collecting resin and then getting clogged by partially cured resin. I resolved this by doing a partial wash, removing the supports and then finishing the wash.


## The Rest

The rest of the build is relatively straight forward. The switches are hot swappable, so you just put them in and the keycaps sit right on top. The only place other than the finishing where I got caught up was on the flashing, which is detailed in flashing.md.

## Notes

### Keycaps

I recommend uisng [Cherry profile keycaps](https://www.wiki.1upkeyboards.com/doku.php?id=knowledge_base:keycap_profiles). You will see a lot of images of let's split keyboards with [DSA keycaps](https://www.wiki.1upkeyboards.com/doku.php?id=knowledge_base:keycap_profiles). They look good, but, since they are all the same profile and do not have identifiers on the f and j keys, I found my fingers got lost on the keyboard when I used them. I moved back to using cherry keycaps and have had a much easier time.
